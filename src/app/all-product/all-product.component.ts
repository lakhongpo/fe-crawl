import { Component, OnInit } from '@angular/core';
import { DataService } from '../../service/data.service';

@Component({
  selector: 'app-all-product',
  templateUrl: './all-product.component.html',
  styleUrls: ['./all-product.component.scss']
})
export class AllProductComponent implements OnInit {
  itemList$: any;

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.updatePriceStr();
  }

  updatePriceStr() {
    this.dataService.getAllProducts(88201679, 30, 0, 2).subscribe((data) => {
      this.itemList$ = data.map((item: any) => {
        item.item_basic.price_before_discount = item.item_basic.price_before_discount
          .toString()
          .slice(
            0,
            item.item_basic.price_before_discount.toString().length - 5
          );
        item.item_basic.price = item.item_basic.price
          .toString()
          .slice(0, item.item_basic.price.toString().length - 5);
        return item.item_basic;
      });
    });
  }

}
