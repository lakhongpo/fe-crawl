import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllProductComponent } from './all-product/all-product.component';
import { MainPageComponent } from './main-page/main-page.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProductByCategoryComponent } from './product-by-category/product-by-category.component';

const routes: Routes = [
  {
    path: 'main',
    component: MainPageComponent,
    children: [
      {
        path: 'all-products',
        component: AllProductComponent,
      },
      {
        path: 'product-by-cate/:cateid',
        component: ProductByCategoryComponent,
      },
    ],
  },
  { path: '',   redirectTo: '/main/all-products', pathMatch: 'full' },
  {
    path:"**",
    component:PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
