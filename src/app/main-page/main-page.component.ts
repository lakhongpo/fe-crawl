import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbConfig } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { DataService } from '../../service/data.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],
})
export class MainPageComponent implements OnInit {
  itemList$: any;
  cateList: any;

  constructor(private dataService: DataService,private router: Router) {}

  ngOnInit(): void {
    this.updatePriceStr();
    this.dataService.getAllCates(88201679, 30, 0, 2).subscribe((data) => {
      this.cateList = data;
      console.log(data);
      
    });
  }

  updatePriceStr() {
    this.dataService.getAllProducts(88201679, 30, 0, 2).subscribe((data) => {
      this.itemList$ = data.map((item: any) => {
        item.item_basic.price_before_discount = item.item_basic.price_before_discount
          .toString()
          .slice(
            0,
            item.item_basic.price_before_discount.toString().length - 5
          );
        item.item_basic.price = item.item_basic.price
          .toString()
          .slice(0, item.item_basic.price.toString().length - 5);
        return item.item_basic;
      });
    });
  }

  navigationtoGetProductCate(cateid:number,name:string){
    this.dataService.updateCateTitle(name);
    this.router.navigate(['/main/product-by-cate', cateid ]);
  }
}
