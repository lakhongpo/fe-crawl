import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainPageComponent } from './main-page/main-page.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { AllProductComponent } from './all-product/all-product.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProductByCategoryComponent } from './product-by-category/product-by-category.component';

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    AllProductComponent,
    PageNotFoundComponent,
    ProductByCategoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    CommonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
