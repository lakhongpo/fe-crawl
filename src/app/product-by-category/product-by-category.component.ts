import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/service/data.service';

@Component({
  selector: 'app-product-by-category',
  templateUrl: './product-by-category.component.html',
  styleUrls: ['./product-by-category.component.scss'],
})
export class ProductByCategoryComponent implements OnInit {
  cateid?: any;
  itemCateList$?:any;
  cateTitle?:string;

  constructor(private route: ActivatedRoute,private dataService:DataService) {}

  ngOnInit(): void {
    
    this.route.paramMap.subscribe((params) => {
      this.cateid = params.get('cateid');
      this.updatePriceStr();
      this.cateTitle= this.dataService.getCateTitle();
    });
  }

  updatePriceStr() {
    this.dataService.getProductsByCate(88201679, 30, 0,this.cateid , 2).subscribe((data) => {
      this.itemCateList$ = data.map((item: any) => {
        item.item_basic.price_before_discount = item.item_basic.price_before_discount
          .toString()
          .slice(
            0,
            item.item_basic.price_before_discount.toString().length - 5
          );
        item.item_basic.price = item.item_basic.price
          .toString()
          .slice(0, item.item_basic.price.toString().length - 5);
        return item.item_basic;
      });
    });
  }
}
