import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  //localhost:4000
  private baseUrl : string = environment.baseUrl;
  private cateTitle ?:string;
  constructor(private http: HttpClient) {}

  updateCateTitle(title:string){
    this.cateTitle=title;
  }

  getCateTitle(){
    return this.cateTitle;
  }

  getAllProducts(shop: number, limit: number, offset: number, retry: number):Observable<any> {
    return this.http.get(
      `${this.baseUrl}/api/get_all_items?shop=${shop}&limit=${limit}&offset=${offset}&retry=${retry}`
    );
  }

  getProductsByCate(shop: number, limit: number, offset: number, cateid:number, retry: number):Observable<any> {
    return this.http.get(
      `${this.baseUrl}/api/get_items_by_cate?shop=${shop}&limit=${limit}&offset=${offset}&cateid=${cateid}&retry=${retry}`
    );
  }

  getAllCates(shop: number, limit: number, offset: number, retry: number) {
    return this.http.get(`${this.baseUrl}/api/get_all_cates?shop=${shop}&limit=${limit}&offset=${offset}&retry=${retry}`);
  }
}
